# boxy-kanban

A kanban board within a boxy diagram!

## STATUS: UNIMPLEMENTED+UNSTABLE

## Concepts

A kanban board is a visual representation of a workflow. "Cards" move
through the diagram from a backlog to different stages of processing
and finally to an archive. Kanban boards are an effective tool to
diagnose bottlenecks, limit WIP (work-in-progress), and effectively
implement continuous improvement.

### Lead/Cycle time

From the moment a card is created, a timer is started to see how long
it takes to be completed or cancelled. The time from creation to
completion is called the 'lead time' while the time the card actually
spent on the board (not in the backlog) is known as the 'cycle
time'. Crucially, this is not the time spent working on a card; it is
an absolute difference between the completion and creation times.

### Swimlanes

In a typical swimming pool, lanes are marked one through eight, with
the fastest swimmers sticking to lane eight while the slower swimmers
and aqua-aerobics stick to lane one. If a swimmer is getting lapped
repeatedly, they may be asked to move to the left.

In boxy-kanban, a swimlane can be determined when creating a card. It
is similar to an Org 'effort' estimate in that it represents the
'target cycle time' of a card, or the amount of time you think the
card will exist on the board. As a card approaches its target cycle
time, as determined by the swimlane, the color will gradually become
more red.

### Checking in/out

A card can be checked out to indicate that it is currently being
processed. A message will be displayed in the modeline, and, if the
kanban board is shared, others will see who is working on the card and
will be prevented from checking out the card or moving it to a
different stage.

### WIP

Work-in-progress can be thought of as how many balls a juggler has in
the air at one time. Limits can be set to make sure a board is not
overloaded with cards. WIP can be limited on a board-by-board basis,
meaning the total number of cards cannot be greater than this limit,
on a per-lane basis, in which each swimlane can have a certain number
of cards, and finally per-stage, which requires additional explanation.

Setting `wip-strategy` to 'by-stage will not block any card from
moving to any stage. Cards should always accurately reflect their
current status. However, cards in a stage directly preceding a 'full'
stage cannot be checked out, and, if the first stage is full, items
from the backlog cannot be moved onto the board.

## Usage

boxy-kanban boards can be backed by either an Org file or a database
file with the extension '.kb', typically.

### With an org file

By default, the first TODO keyword will be used as the backlog and all
DONE keywords will be used as the archive. All other TODO keywords
will be used as process stages in a workflow. For example, the default
keywords:

```
#+TODO: TODO | DONE
```

would not allow for any card to exist on the kanban board. On the
other hand,

```
#+TODO: LEAD QUALIFYING ASSESSING PROPOSING NEGOTIATING | CLOSED LOST
```

would allow all cards with the `QUALIFYING`, `ASSESSING`, or
`PROPOSING` keywords to exist on a kanban board.

...

The command `boxy-kanban-show` will search up the hierarchy of the Org
file for the property `:KANBAN_BOARD: t` in a heading starting from
point and display the first kanban board it sees, or displays the
entire file as a kanban board if no such property exist on any
headings.

Alternatively, if the file is not currently open, the command
`boxy-kanban-open` can be directed to an Org file, in which it will
read all headings with the `:KANBAN_BOARD: t` property and allow you
to choose which one to display, or display the entire file as a kanban
board if no such property exists.

### With a database

Use the command `boxy-kanban-create` to create a new database-backed
kanban file. The command will ask for a filepath to the kanban board
to create. If you choose a .org filepath, an Org-backed kanban board
will be created. For all other file extensions, including '.kb', a
lisp database file will be created at the requested filepath.

Once a kanban file exists, the command `boxy-kanban-open` can be
directed to it to open an existing board.

### Options

| option                     | description                                | default |
|----------------------------|--------------------------------------------|---------|
| boxy-kanban-identity       | The identity to use to check out/in a card | nil     |
| boxy-kanban-swimlane-units | Either 'minute, 'hour, or 'day             | 'hour   |

### Board structure and constraints

The command `boxy-kanban-edit` from within a board will ask for an
attribute to edit and will either save the supplied value in the
database or edit the properties of the current board in an Org file.

| attribute      | default variable                   | Org property                         | short description                                                                                                                       |
|----------------|------------------------------------|--------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------|
| stages         | boxy-kanban-default-stages         | #+TODO:                              | A list of states that a card can be in                                                                                                  |
| backlog-stage  | boxy-kanban-default-backlog-stage  | #+TODO:                              | A singular stage where cards exist before being brought onto the board and starting their 'cycle time'                                  |
| archive-stages | boxy-kanban-default-archive-stages | #+TODO:                              | A list of possible final states where cards can go after they leave the board                                                           |
| swimlanes      | boxy-kanban-default-swimlanes      | #+PROPERTY: Effort_ALL, :Effort_ALL: | A list of numbers from largest to smallest that represent available target 'cycle' times to choose from when creating a new card        |
| wip-limits     | boxy-kanban-default-wip-limits     | :KANBAN_WIP_LIMITS:                  | Either a list of numbers or a single number depending on the value of 'wip-strategy'. The numbers represent the maximum number of cards |
| wip-strategy   | boxy-kanban-default-wip-strategy   | :KANBAN_WIP_STRATEGY:                | Either 'by-lane', 'by-board', or 'by-stage', described above                                                                            |
